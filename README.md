# First Story Detection using Entities and Relations [Coling-2016], [Journal Version - 2018] 

### First-Story Annotator
This tool allows you to quickly annotate events from a text corpus using the left and the right click. It requires a MongoDB database
that contains the documents under annotation.


To start with, you should install

* Apache2 Server
* MongoDB
* PHP5


### MongoDB:
* You should import the documents of your corpus in the MongoDB database
* For each document you need to have at least the columns {Title, Content}
* You need to have a Text index on the title of your MongoDB collection


### How to run?

* Create your MongoDB collection
* Copy the www folder to your www/<my-favorite-folder> directory
* Just use your browser and start the annotation!

### Important!
You need to configure the settings inside the file www/init.php according to your MongoDB configuration.


## Datasets Available

For access to the dataset Google News dataset (D2) please contact the authors of the paper:

** Karkali, Margarita, et al. "Efficient online novelty detection in news streams." International Conference on Web Information Systems Engineering. Springer Berlin Heidelberg, 2013. **

If you intent to use the Google-News Dataset please ** ensure ** that you cite the above paper where the dataset was originaly created.

## VM Available

All the  datasets that can be shared are available in the VirtualBox VM provided in URL. The virtual machine includes the artifacts required in order to rerun the paper experiments or to run the algorithms  available on new data.
Again remember to cite:

** Karkali, Margarita, et al. "Efficient online novelty detection in news streams." International Conference on Web Information Systems Engineering. Springer Berlin Heidelberg, 2013. **

if you use the  Google News datasets.

* VM credentials
 ** Username: nikos
 ** Password: journal

## Dataset Parser

If you intend to reprocess your data in order to extract the metadata required from our work you may use the executable src/jar/GenericPreprocessor.jar. 
You need to have your corpus in the following format:

<id 1> <title 1> <content 1> <timestamp publish 1>

<id 2> <title 2> <content 2> <timestamp publish 2>

...

<id N> <title N> <content N> <timestamp publish N>

### How to run?

java -Xmx8g -jar GenericPreprocessor.jar <input file> <output file> <separator>

The parser is standalone. All the required models for the preprocess are included.