<?php
ini_set('display_errors',1);
session_start();
?>

<html>
<head>
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.3.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
</script>
<script type="text/javascript">
function Accept(id)
{
	//alert('Accept');
	$.ajax({
		'type': 'GET',
        'url': 'accept.php',
        'data': {
           id:id,
           accept:"True"
        }
	}
	);
}
function Reject(id)
{
	//alert('Reject');
	$.ajax({
		'type': 'GET',
        'url': 'accept.php',
        'data': {
           id:id,
           accept:"False"
        }
    });
}
function notNews(id)
{
	//alert('Not-News');
	$.ajax({
		'type': 'GET',
        'url': 'notNews.php',
        'data': {
           id:id,
           accept:"False"
        }
    });
}
function unsure(id)
{
	//alert('Unsure');
	$.ajax({
		'type': 'GET',
        'url': 'unsure.php',
        'data': {
           id:id,
           accept:"False"
        }
    });
}
function clicks()
{
	$('tr').click(function(){
	    	//alert('click');
			var source=$(this).attr('id');
			Accept(source);
			var row = document.getElementById(source);
		    row.remove();
		})
		.mousedown(function(e) //Right click
	    {
	        if(e.which == 3) //1: left, 2: middle, 3: right
	        {
	            var source=$(this).attr('id');
	            Reject(source)
				var row = document.getElementById(source);
			    row.remove();
	            return false;
	        }
	    });
}
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": {
        'type': 'GET',
        'url': 'query.php',
        'data': {
           keyword: '<?php echo($_GET["keyword"]);?>'
        }
       	},
       	"columns": [
            { "data": "date" },
            { "data": "title" },
            { "data": "size" },
            { "data": "notEvent" },
            { "data": "unsure" }
        ],
       	'rowId': 'id'
    } );
    $(window).load(function() {
	    $('tr').click(function(){
	    	//alert('click');
			var source=$(this).attr('id');
			Accept(source);
			var row = document.getElementById(source);
		    row.remove();
		})
		.mousedown(function(e) //Right click
	    {
	        if(e.which == 3) //1: left, 2: middle, 3: right
	        {
	            var source=$(this).attr('id');
	            Reject(source)
				var row = document.getElementById(source);
			    row.remove();
	            return false;
	        }
	    });
	});
	setInterval(clicks,1000);
} );
</script>

<script type="text/javascript">
$( document ).ready(function() {
$('tr').click(function(){
	var source=$(this).attr('id');


	Accept(source);

	var row = document.getElementById(source);
    row.remove();
	})
	.mousedown(function(e) //Right click
    {
        if(e.which == 3) //1: left, 2: middle, 3: right
        {
            var source=$(this).attr('id');


            Reject(source)
			var row = document.getElementById(source);
		    row.remove();
            return false;
        }
    });
    //$('body').on('contextmenu', 'img', function(e){ return false; });
});


</script>

<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<link rel="stylesheet" type="text/css" href="/media/css/site-examples.css?_=b05357026107a2e3ca397f642d976192">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
</head>
<body>
<?php echo($_GET['keyword']);?>
<h2>Event-Driven Annotation</h2>
<p>
Bellow there is the list of documents that received according to your keyword.
Please find the first document that <b>REALLY</b> represents the story and left click on the row. 
An eventId is generated on the background. Please use the right click in order to add additional documents to the event.
</p>
<h2>Important:</h2>
<p>
If you find a new event you may use again the right click in order to generate a new eventId. However, from this point when you
use the left click the documents are added to the new event and not to the first!.
</p>
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Size</th>
                <th>Not-News</th>
                <th>Unsure</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Size</th>
                <th>Not-News</th>
                <th>Unsure</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>
