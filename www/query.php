<?php
ini_set('display_errors',1);
require("init.php");
$keyword=$_GET['keyword'];
$query=array();
if (strcmp($keyword,"all")==0)
{
	$query['eventId']=array('$exists'=>false);
	$query['notNews']=array('$exists'=>false);
	$query['unsure']=array('$exists'=>false);
	$cursor = $col->find($query);
}
else
{
	$query['eventId']=array('$exists'=>false);
	$query['notNews']=array('$exists'=>false);
	$query['unsure']=array('$exists'=>false);
	$query['$text']=array('$search'=>$keyword);
	//print_r($query);
	$cursor = $col->find($query);
}
$cursor->sort(array("timestampPub"=>1))->limit(10000);
$data=array();
$rowId=array();
foreach ($cursor as $document) {
    //echo $document["title"] . "\n";
    $object=array();
    $object['date']=date('m/d/Y h:i:s', intval($document['timestampPub'])/1000);
    $object['title']=$document['title'];
    $object['size']=strlen($document['title'].$document['content']);
    $object['notEvent']='<a href="javascript:notNews('.$document['_id'].');">Not-News</a>';
    $object['unsure']='<a href="javascript:unsure('.$document['_id'].');">Unsure</a>';
    $object['id']=$document['_id'];
    $data[]=$object;
    $rowId[]=$document['_id'];
}
$response=array("data"=>$data,"rowId"=>$rowId);
echo(json_encode($response));
?>
