<?php
ini_set('display_errors',1);
require("init.php");
$id=intval($_GET['id']);
$query=array("_id"=>$id);

//print_r($query);
$cursor = $col->find($query);
$cursor->sort(array("timestampPub"=>1))->limit(10000);
$data=array();
$rowId=array();
foreach ($cursor as $document) {
    //echo $document["title"] . "\n";
    $relations=$document['relations'];
    foreach ($relations as $relation)
    {
        $object=array();
        $object['arg1']=$relation['arg1'];
        $object['relStr']=$relation['relStr'];
        $object['arg2']=$relation['arg2'];
        $data[]=$object;
    }
    break;
}
$response=array("data"=>$data,"rowId"=>$rowId);
echo(json_encode($response));
?>
