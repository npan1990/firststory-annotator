<?php
ini_set('display_errors',1);
require("init.php");
$event=$_GET['event'];
$query=array();
if(strcmp($event,"true")!=0)
{
    $keyword=$_GET['keyword'];
    $query['$text']=array('$search'=>$keyword);
    $query['eventId']=array('$exists'=>false);
    $query['notNews']=array('$exists'=>false);
    $query['unsure']=array('$exists'=>false);
}
else
{
    $query['eventId']=array('$exists'=>true);
    $query['firstStory']="True";	
    $query['notNews']=array('$exists'=>false);
    $query['unsure']=array('$exists'=>false);
}
//print_r($query);
$cursor = $col->find($query);
if(strcmp($event,"true")!=0)
{
	$cursor->sort(array("timestampPub"=>1))->limit(60000);
}
else
{
	$cursor->sort(array('eventId' => -1));
}
$data=array();
$rowId=array();
foreach ($cursor as $document) {
    //echo $document["title"] . "\n";
    $object=array();
    $object['date']=date('m/d/Y h:i:s', intval($document['timestampPub'])/1000);
    $object['title']=$document['title'];
    $object['size']=strlen($document['title'].$document['content']);
    if(strcmp($event,"true")==0)
    {
        $object['update']='<a href="updateEvent.php?eventId='.$document['eventId'].'">Update</a>';
        $object['view']='<a href="viewEvent.php?eventId='.$document['eventId'].'">View Event</a>';
	$object['reject']='<a href="javascript:reject('.$document['eventId'].');">Reject</a>';
    }
    else
    {
        $object['accept']='<a href="javascript:Accept('.$document['_id'].');">Accept</a>';
        $object['reject']='<a href="javascript:Reject('.$document['_id'].');">Reject</a>';
    }   
    $object['id']=$document['_id'];
    $data[]=$object;
    $rowId[]=$document['_id'];
}
$response=array("data"=>$data,"rowId"=>$rowId);
echo(json_encode($response));
?>
