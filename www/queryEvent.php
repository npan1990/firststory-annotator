<?php
ini_set('display_errors',1);
require("init.php");
$eventId=$_GET['eventId'];
$query=array();
$query['eventId']=intval($eventId);
//print_r($query);
$cursor = $col->find($query);
$cursor->sort(array("timestampPub"=>1))->limit(10000);
$data=array();
$rowId=array();
foreach ($cursor as $document) {
    //echo $document["title"] . "\n";
    $object=array();
    $object['date']=date('m/d/Y h:i:s', intval($document['timestampPub'])/1000);
    $object['title']=$document['title'];
    $object['size']=strlen($document['title'].$document['content']);
    $object['viewRelations']='<a href="viewRelations.php?id='.$document['_id'].'">View Relations</a>'; 
    $object['id']=$document['_id'];
    $data[]=$object;
    $rowId[]=$document['_id'];
}
$response=array("data"=>$data,"rowId"=>$rowId);
echo(json_encode($response));
?>
