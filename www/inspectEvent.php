<?php
ini_set('display_errors',1);
session_start();
require("init.php");
$query=array("firstStory"=>"True");
$eventId = intval($col->find($query)->count());
$_SESSION['eventId']=$eventId+1;
?>

<html>
<head>
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.3.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
</script>
<script type="text/javascript">
function clicks()
{
	$('tr').click(function(){
	    	//alert('click');
			var source=$(this).attr('id');
			Accept(source);
			var row = document.getElementById(source);
		    row.remove();
		})
		.mousedown(function(e) //Right click
	    {
	        if(e.which == 3) //1: left, 2: middle, 3: right
	        {
	            var source=$(this).attr('id');
	            Reject(source)
				var row = document.getElementById(source);
			    row.remove();
	            return false;
	        }
	    });
}
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": {
        'type': 'GET',
        'url': 'queryEvents.php?event=true',
        'data': {
           keyword: ''
        }
       	},
       	"columns": [
            { "data": "date" },
            { "data": "title" },
            { "data": "size" },
            { "data": "update" }
        ],
       	'rowId': 'id'
    } );
} );
</script>
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
<link rel="stylesheet" type="text/css" href="/media/css/site-examples.css?_=b05357026107a2e3ca397f642d976192">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
</head>
<body>
<h2>Select an Event</h2>
<p>
After selecting one of the existing events you may proceed and add more documents for the event.
</p>
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Size</th>
                <th>Update</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Size</th>
                <th>Update</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>
